# Тестовое задание для фирмы MetaQuotes. 

Выполнял в августе 2015. Описание тестового в файле **Тестовое задание.docx** в корне проекта.

#### Документация
* [Тестовое задание в wiki](https://gitlab.com/23Alex24/interview.metaquotes/blob/master/docs/home.md) и в файле Тестовое задание.docx в корне
* [Скриншоты приложения](https://gitlab.com/23Alex24/interview.metaquotes/blob/master/docs/screenshots.md)
* [Техническая документация](https://gitlab.com/23Alex24/interview.metaquotes/blob/master/docs/technical_docs.md)

#### Использованные технологии и подходы
* БД - SQL SERVER Compact Edition, файл БД сохраняется в папку AppData в проекте
* Луковая архитектура (Onion architecture), Dependency Injection (Autofac)
* Локализация через файлы ресурсов .resx
* C#, .NET Framework 4.5
* Html, Css, razor, Bootstrap 3.3, JQuery 2.1.4, gridmvc.js

#### Запуск
* Запускать в VS 2015 или VS 2017
* Для запуска БД и скриптов выполнять никаких не надо, при запуске автоматом будет создана и предзаполнена БД пользователями

#### Пользователи для входа: 

**Логин**: user1 **Пароль**:123123  НЕ администратор

**Логин**: user2 **Пароль**:123123  Администратор

**Логин**: user3 **Пароль**:123123  НЕ администратор

**Логин**: user4 **Пароль**:123123  Администратор