# Скриншоты личного кабинета от имени администратора

#### Администратор видит все заметки

![MetaQuotes12](https://gitlab.com/23Alex24/interview.metaquotes/raw/master/images/MetaQuotes12.png)

#### Пункт меню логи

![MetaQuotes13](https://gitlab.com/23Alex24/interview.metaquotes/raw/master/images/MetaQuotes13.png)

#### Страница логов

![MetaQuotes14](https://gitlab.com/23Alex24/interview.metaquotes/raw/master/images/MetaQuotes14.png)